#
# Input Parameters for KiBaM
#
# Purpose:  Input parameters for KiBaM model
# Author:   Nicolás Conde
# Creation Date:  Nov. 17, 2021
#
import Functions as fc
import numpy as np

"""Condiciones iniciales y contorno"""
NSeries     = 10
NParallel   = 6
SoC0        = 100-1e-5
TempBat     = 268
Thost       = 270 # K

"""Condiciones simulación"""
PowerIn     = 0
PowerOut    = 600
dt          = 10

class CurrentStatusState:
    def __init__(self, t, current, C, param, T, V, SoC, num, P):
        # current list (Actual current charge or discharge, maximum ratings)
        self.I                  = current
        self.MaxRateCharge      = 3*num[1]
        self.MaxRateDischarge   = 3*num[1]
        self.StateOfCharge      = 0
        # capacity list (Available, Unavailable, Remaining, Max, State of Charge)
        self.cav                = C[0]
        self.cunav              = C[1]
        self.crem               = C[2]
        self.cmax               = C[3]
        self.soc                = SoC
        # model parameter lists (KiBaM, nd = rate charge/discharge, Temp parameters)
        self.c                  = param[0]
        self.k                  = param[1]
        self.nd                 = param[2]
        # temperature
        self.T                  = T
        # voltage battery
        self.V                  = V
        # Pack (Voltage, cells in series, cells in parallel)
        self.Vpack              = 5*num[0]
        self.NSeries            = num[0]
        self.NParallel          = num[1]
        # Power balance list (Power in from solar panel, power out to load, power supplied, power dissipated)
        self.pIn                = P[0]
        self.pOut               = P[1]
        self.pSupp              = P[2]
        self.pDiss              = P[3]
        # Ciclo de Vida
        self.LC                 = 0
        self.LCsoc              = 1
      
        """ Control térmico de las celdas """
        self.Rint               = 0.0029                        # Ohm - Resistencia interna
        self.m                  = 46 / 1000                     # kg - masa  de cada celda
        self.Cp                 = 895                           # J / kg K - Capacidad calorífica de cada celda
        self.e                  = 0.072                         # Niquel - Emisividad
        self.Aexp               = 65 * 9 * np.pi * 2 / 1000**2  # m2 - Area expuesta de la celda para radiar
        self.stefan             = 5.67e-8                       # W / m2 K - Stefan Blotzmann
        self.kpcb               = 0.3                           # W / m K - Conductividad térmica del PCB
        self.Apcb               = 20*1.6/1000**2                # Area conducción PCB

def BatteriesPackageInit(T0, NBatParallel, NBSeries, PowerIn, PowerOut, SoC0):
    """
    Initialization of the battery pack. An initial temperature,
    the number of cells in series and parallel, the input and output powers, the 
    empirical battery models are loaded and the battery coefficients are defined.
    The state vector with all initial values is created.

    Parameters
    ----------
    T0 : int
        initial temperature.
    NBatParallel : int
        number of cells in parallel.
    NBSeries : int
        number of cells in series.
    PowerIn : float
        power in from the power generation block.
    PowerOut : float
        power out from the load block.

    Returns
    -------
    state0 : class
        state vector for the start of the simulation.
    PH : float
        power left over from the balance.
    """
    y_cmax = [5.68341143e-05, -6.58895161e-02,  2.83271679e+01, -5.33886247e+03, 3.73074290e+05]
    
    Cmax = np.poly1d(y_cmax)(T0)*NBatParallel/1000 # Ah

    # Params Voltage model
    V0 = 50 # V - E0
    
    # Empirical parameters
    c = 0.89
    k = 0.017
    nd = 1.15 # Charge / Discharge efficiency
    
    """ Initial State """
    i0,PowerDiss,PowerSupp = fc.PowerLoss(PowerIn, PowerOut, SoC0, V0*NBSeries, Cmax, 20, 20)
    state0 = CurrentStatusState(0, i0, [Cmax*SoC0,0,Cmax*SoC0,Cmax], [c,k,nd], T0, 4, SoC0, [NBSeries,NBatParallel], [PowerIn,PowerOut,PowerSupp, PowerDiss])
    
    return state0

bateries_state = BatteriesPackageInit(TempBat, NParallel, NSeries, PowerIn, PowerOut, SoC0) # Temp , Bat paralel, Bat serie, power in, power out

if __name__ == "__main__":
    if bateries_state:
        print("State object created")
    else:
        print("Error in state object creation")
