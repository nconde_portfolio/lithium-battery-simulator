# -*- coding: utf-8 -*-
#
# Kinetic Batterie Model
#
# Purpose:  Generate the charge and discharge curves for a lithium battery package.
# Author:   Nicolas Conce (Colomb Institute)
# Creation Date:  Nov. 20, 2020
# Modification Date: Nov. 17, 2021
# Version: 1.4
#
import numpy as np
import matplotlib.pyplot as plt
import Functions as fc
from parameters import bateries_state, BatteriesPackageInit, dt
from tqdm import tqdm               # Barra de progreso

def state_propagator(state, PowerIn, PowerOut, TempBat, dt):
    """
    Takes the input and output power, sets a C/D CurrentStatus, 
    and evolves by taking that CurrentStatus as a constant until 
    it is fully charged or fully discharged. It returns the 
    vectors to plot the evolution.
    
    Parameters
    ----------
    PowerIn : float
        Power in from Power Generation block.
    PowerOut : float
        Power Out from Power Consumption block.
    TempBat : float
        Temperature of the batteries from Thermal block.
    state : class
        State Vector.
    
    Returns
    -------
    state
    """
    # Stops if final time is achieved or if battery is totally discharged.
    """ CurrentStatus state actualized """
    state.T = int(TempBat)
    state.pIn = PowerIn
    state.pOut = PowerOut
    
    y_cmax = [5.68341143e-05, -6.58895161e-02,  2.83271679e+01, -5.33886247e+03, 3.73074290e+05]
    
    state.cmax = np.poly1d(y_cmax)(state.T)*state.NParallel/1000-state.LC # Ah
    
    state.I, state.pDiss, state.pSupp = fc.PowerLoss(state.pIn, state.pOut, state.soc, 
                                                  state.Vpack, state.cmax, 
                                                  state.MaxRateCharge, 
                                                  state.MaxRateDischarge)
    state = fc.Cycles(state)
    state.crem = fc.CRemaining(dt, state)
    state.cunav = fc.CUnavailable(dt, state)
    state.cav = fc.CAvailable(dt, state.crem, state.cunav)
    state.soc = (state.crem-state.cunav)*100/state.cmax
    state.V = fc.Battery_voltage(state)
    return state

def test_voltage_vs_temperature():

    """Condiciones iniciales y contorno"""
    NSeries     = 1
    NParallel   = 3
    SoC0        = 100-1e-5
    TempBat     = 268
    Thost       = 270 # K

    """Condiciones simulación"""
    PowerIn     = 0
    PowerOut    = 10
    dt          = 10

    print("Simulación de Tensión Vs Temperaturas...\n")

    testsoc     = [0,5,20,40,60,80,100]
    testv55     = [2.7,3.2,3.5,3.6,3.7,3.8,4.1]
    testv_20    = [2.7,2.8,3.0,3.2,3.3,3.4,3.8]

    V_, SOC_    = [],[]
    plt.figure(figsize=(10,6))
    for T in tqdm(range(250,325,5)):
        state = BatteriesPackageInit(T, NParallel, NSeries, PowerIn, PowerOut, SoC0) # Temp , Bat paralel, Bat serie, power in, power out
        V_,SOC_ = [],[]
        while state.V>3:
            state = state_propagator(state, PowerIn, PowerOut, T, dt)
            V_ += [state.V]
            SOC_ += [state.soc]
        plt.plot(SOC_,V_,linewidth=0.2, c='k', label=str(T))

    plt.plot(testsoc,testv55,linewidth=0.2, c='b',label="Datasheet(55°C)") ; plt.scatter(testsoc,testv55,s=0.4,c='b')
    plt.plot(testsoc,testv_20,linewidth=0.2, c='r',label="Datasheet(-20°C)") ; plt.scatter(testsoc,testv_20,s=0.4,c='r')
    plt.legend()
    plt.xlabel("SoC [%]")
    plt.ylabel("Tensión [V]")
    plt.title("Tensión de batería vs SoC para distintas temperaturas")
    plt.show()

def test_battery_operation(mode = 0):
    V_,SOC_, Cav = [],[],[]
    while bateries_state.soc>50.63:
        state = state_propagator(bateries_state, bateries_state.pIn, bateries_state.pOut, bateries_state.T, dt)
        Cav += [state.cav]
        V_ += [state.V*bateries_state.NSeries]
        SOC_ += [state.soc]

    plt.plot(np.arange(0,len(SOC_))*dt/60,SOC_)
    plt.xlabel('Time [min]')
    plt.ylabel('SoC [%]')
    plt.title("State of Charge")
    plt.grid(color='k', linestyle='-', linewidth=0.2)
    plt.ylim([0,100])
    plt.show()

    plt.plot(np.arange(0,len(Cav))*dt/60,Cav)
    plt.xlabel('Time [min]')
    plt.ylabel('Cav [Ah]')
    plt.title("Capacity Available")
    plt.grid(color='k', linestyle='-', linewidth=0.2)
    #plt.ylim([0,100])
    plt.show()

    plt.plot(np.arange(0,len(V_))*dt/60,V_)
    plt.xlabel('Time [min]')
    plt.ylabel('V [V]')
    plt.title("Voltage")
    plt.grid(color='k', linestyle='-', linewidth=0.2)
    #plt.ylim([0,50])
    plt.show()
    return


if __name__ == "__main__":
    #test_voltage_vs_temperature()
    test_battery_operation()

    # You can build your own operation concept by passing PowerOut and PowerIn to the state_propagator function.
    # In test_battery_operation there is a very simple concept of operation, where you have a constant load.
