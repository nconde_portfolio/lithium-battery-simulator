import numpy as np
import matplotlib.pyplot as plt
def CUnavailable(dt, s):
    """
    Es la capacidad que existe en la batería, pero que no se puede usar. 
    Energía inutil. Aumenta con la corriente de descarga y el tiempo.
    
    Parameters
    ----------
    t : float
        time instant.
    s : class
        State Vector. See CurrentStatusState class.

    Returns
    -------
    float
    CurrentStatus unavailable capacity actualized.
    
    If is in discharge equation CurrentStatus dependent shall be used.
    If CurrentStatus is 0, battery suffer a recovery, this is because 
    I use the "if".
    """
    if s.cunav >= 0:
        serie = 0
        for i in range(0,10):
            serie += (-s.k*dt)**i/np.math.factorial(i)
            
        if abs(s.I) > 0:
            return s.cunav+s.I*(1/s.c-1)*(1-serie)/s.k/3600/2
        else:
            return s.cunav+s.cunav*serie
    else:
        return 0

def CRemaining(dt, s):
    """
    Toma la capacidad disponible, le suma la capacidad cargada/descargada.
    Si esa capacidad es mayor a la máxima, devuelve la máxima. 
    Si es menor, devuelve esa capacidad restante.
    
    Parameters
    ----------
    dt : int
        Delta time since last actualization in seconds.
    s : class
        State Vector.

    Returns
    -------
    float
        Capacity remaining in battery.

    """
    if s.I >0:
        C = s.crem-dt*s.I*s.nd/3600
    else:
        C = s.crem-dt*s.I/s.nd/3600 
        
    if C < s.cmax:
        return C
    else:
        return s.cmax

def CAvailable(dt, crem, cunav):
    """
    Es la capacidad que si se puede usar para alimentar un circuito. Es la
    capacidad restante, menos la capacidad que no podemos usar.
    
    Parameters
    ----------
    t : int
        CurrentStatus time in seconds.
    s : class
        State Vector. See CurrentStatusState class definition.

    Returns
    -------
    float
        CurrentStatus available capacity actualized.

    """
    return crem - cunav

def Battery_voltage(s):
    """
    Es el modelo de carga y descarga en tensión para una batería de litio ion.
    Utiliza una lista de parámetros determinados experimentalmente ajustando
    la curva a las curvas del datasheet de una batería. Y que varían según la
    temperatura.
    
    Parameters
    ----------
    state:
        State Vector.

    Returns
    -------
    CurrentStatus voltage.

    """
    param55 = [3.735, -0.755, 0.448, -1.352, 1.734, 0.7]
    param_20 = [2.8, -0.555, 0.248, -1.22, 2.44, 0.95]
    p = []
    for i in range(0,len(param55)):
        p += [param_20[i]+(param55[i]-param_20[i])*(s.T-250)/75]
        
    if s.soc > 100 or s.soc <=0:
        return s.V
    else:
        return p[0]+p[1]*(-np.log(s.soc/100))**p[2]+p[3]*s.soc/100+p[4]*np.e**(p[5]*(s.soc/100-1))

def PowerLoss(PowerIn, PowerOut, SoC, Vpack, Cmax, MaxRateOfCharge, MaxRateOfDischarge):
    """
    Balance de potencia. Se usa potencia para el circuito, para cargar las celdas
    y para descartar el sobrante. Si falta potencia, la batería lo suministra hasta
    superar un umbral.

    Parameters
    ----------
    PowerIn : float
        potencia que entra.
    PowerOut : float
        potencia que sale.
    SoC : float
        estado de carga de la celda.
    Vpack : float
        voltaje de todo el pack.
    Cmax :  float
        capacidad máxima del pack.
    MaxRateOfCharge : float
        carga máxima tolerable.
    MaxRateOfDischarge : float
        descarga máxima tolerable.

    Returns
    -------
    -I : float
        corriente negativa si es carga, positiva si es descarga, 0 si ninguna.
    PH : float
        potencia descartada como calor.
    PS : float
        potencia suministrada por el paquete de baterias al circuito.

    """
    # Regulator Efficiency
    nr = 0.94
    
    DeltaP = PowerIn - PowerOut
    PH = 0
    PS = DeltaP 
    if DeltaP > 0: #Charge situation
        I = DeltaP/Vpack*nr+1e-3
        if SoC < 100:
            if I > MaxRateOfCharge:
                PH = Vpack*(I-MaxRateOfCharge)
                I = MaxRateOfCharge
                PS = I*Vpack
        else:
            I = 1e-3
            PH = DeltaP
            PS = 0
    else: # Discharge
        I = DeltaP/Vpack/nr+1e-3
        if I > MaxRateOfDischarge:
            #Turn off satellite
            I = 1e-3
            PH = DeltaP
        PS = I*Vpack    
    return -I, PH, PS

def Cycles(s):
    """
    Degradación de la batería por ciclos.

    Parameters
    ----------
    s : class
        State Vector.

    Returns
    -------
    s : class
        State Vector.

    """
    if s.I > 0 and s.StateOfCharge == 1:
        s.StateOfCharge = 0
        # print(s.cmax)
        s.LC += 0.2*s.cmax/(2731.7*(1-s.LCsoc)**(-0.679)*np.e**(1.614*s.LCsoc)-1)
        # print(s.LC)
    elif s.I < 0 and s.StateOfCharge == 0:
        s.StateOfCharge = 1
        s.LCsoc = s.soc/100
        # print(s.LCsoc)
    return s