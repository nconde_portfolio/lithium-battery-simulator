# Lithium battery simulator

Lithium battery life cycle simulator.

Coded from: https://ieeexplore.ieee.org/document/8243665

A novel fractional-order Kinetic Battery Model (KiBaM) is proposed to accurately describe the battery nonlinear characteristic of the available capacity under discharge. Firstly, the influence of different discharge current on battery available and unavailable capacity is analyzed. Then, with the theory of fractional calculus, a novel KiBaM model model equation is proposed to characterize the battery running time and the battery non-linear capacity effect under discharge, accurately. Finally, the battery capacity influence under different discharge current are compared between the proposed fractional-order KiBaM model and the traditional integer-order model. The simulation results show that the fractional-order model can also describe the battery nonlinear characteristic accurately.
